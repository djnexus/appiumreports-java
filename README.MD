# Generar Reportes de Appium usando ExtentReport Library #


## Requerimientos ##

1. Java.
2. Conocimento de Maven.
3. IDE.
4. Appium.
5. Emulador instalado y funcionando para correr pruebas.

## Proceso ##

### Introducción ###

-Antes de poder generar nuestros reportes necesitaremos crear nuestra suite de pruebas en Appium; se conoce una versión para crear este tipo de pruebas implementadas por otros desarrolladores, sin embargo,
la base de este proyecto es Maven el cual no fue utilizado en las versiones anteriores. Maven nos brindan una estructura de proyecto especializado para pruebas donde tenemos el ambiente definido. Tambien, 
la gran ventaja de Maven es su facilidad de incluir y manejar librerias. Esta demostración incluye la famosa libreria de pruebas TESTNG las cual nos permite manejar nuestras clases de pruebas dentro de la 
Main Class en Java; de esta manera tenemos mas facilidad a la hora de crear y ejecutar pruebas. 

### Instalación de Maven e Creación del Proyecto  ###

-Antes de poder usar Maven tenemos que instalarlo en nuestro sistema e incluirlos en nuestras variables de ambiente, de esta manera podremos crear proyectos completos de Maven usando la consola. A pesar de 
que la gran mayoria de los IDEs modernos incluyen Maven dentro de sus paquetes no es del todo recomendado crear un nuevo proyecto Maven usando el IDE. **Pero por qué si usando el IDE es la formas mas rapida y segura?**
Pues creando el proyecto a traves de IDE y no por Consola podria limitar nuestra Version de JAVA causando que algunas librerias necesarias para la implementacion de las pruebas y los reportes no funcionen de 
manera correcta. 

-Para instalar Maven necesitaremos descargar su archivo descomprimible desde la pagina oficial de Maven [Descargar Maven](https://maven.apache.org/download.cgi). Luego tenemos que buscar **"Files-Binary zip archive"**

-Luego tendriamos que poner la variables globales en la ruta de los archivos que hemos descromprimos. Para mejor entendimiento dejaré adjunto un video de como instalar Maven de Manera conrrecta e incluirlo en las
Varibles de entorno. [Video Tutorial](https://www.youtube.com/watch?v=6AVC3X88z6E).

- Una vez hayamos instalado y comprobado que tenemos Maven en nuestra maquina procedemos a crear nuestro Project de Java en Maven usando la consola:
	1. Abrir la consola. 
	
	2. Copiar y pegar el siguente texto: `mvn archetype:generate -DgroupId=test -DartifactId=appiumtests -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=booleanValue` 
	
	3. Se generarán los archivos necesarios para la creación del proyecto. Cuando se está finalizando la creación tendremos que pulsar "Enter" varias veces para finilizar con el proceso. 
	
	4. Para nombrar el proyecto solo hay que modificar el campo `DartifactId` y agregar el nombre que deseas, lo demas es recomendable dejarlo de la manera que está.
	
	5. Una vez se haya generado el proyecto, en las ultimas lineas podremos visualizar la ruta donde se creó el proyecto. 
	
	6. Abrir el proyecto usando el IDE que gustes.
	
### Instalación dependencias o librerias  ###

-Ya con nuestro proyecto creado y abierto en nuestro IDE, podremos ver en una arbol de archvos un archivo llamado ** pom.xml ** el cual será una especia de `package.json` como en NodeJS. En el cual se pueden instalar
las dependecias necesarias, en nuestro caso vamos a usar las siguientes: 

	1. TestNG
	
	2. Appium Java 


