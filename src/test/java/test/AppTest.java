package test;

import static org.junit.Assert.assertTrue;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;

/**
 * Unit test for simple App.
 */
public class AppTest extends ExtentReportsDemo
{
    AppiumDriver<MobileElement> driver;

    @BeforeTest
    public void setup() {
        try {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("deviceName", "emulator-5554");
            caps.setCapability("platformName", "Android");
            caps.setCapability("platformVersion", "9");
            caps.setCapability("appPackage", "com.android.calculator2");
            caps.setCapability("appActivity", "com.android.calculator2.Calculator");
//        caps.setCapability(MobileCapabilityType.APP, "");
//        caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AppiumDriver<MobileElement>(url, caps);

        } catch (Exception exp) {
            System.out.println("Cause is: " + exp.getCause());
            System.out.println("Message is: " + exp.getMessage());
            exp.printStackTrace();
        }
    }
    @Test
    public void addTest(){
        System.out.println("Application Started...");
        // creates a toggle for the given test, adds all log events under it
         ExtentTest test1 = extent.createTest("Add", "AddUp two numbers and get results into console");
        MobileElement one = driver.findElement(By.id("com.android.calculator2:id/digit_1"));
        MobileElement plus = driver.findElement(By.id("com.android.calculator2:id/op_add"));
        MobileElement seven = driver.findElement(By.id("com.android.calculator2:id/digit_7"));
        MobileElement equal = driver.findElement(By.id("com.android.calculator2:id/eq"));
        MobileElement result = driver.findElement(By.id("com.android.calculator2:id/result"));
        // log(Status, details)
        test1.log(Status.INFO, "Test started...");
        one.click();
<<<<<<< HEAD
        test1.log(Status.PASS, "Clicked on Number One(1)");
        plus.click();
        test1.log(Status.PASS, "Clicked on Plus Sign(+)");
        seven.click();
         test1.log(Status.PASS, "Clicked on Number (7)");
        equal.click();
         test1.log(Status.PASS, "Clicked on Equal Sign(=)");
=======
        test1.log(Status.PASS, "Clicked on Number One");
        plus.click();
        test1.log(Status.PASS, "Clicked on Plus Sign");
        seven.click();
         test1.log(Status.PASS, "Clicked on Number Seven");
        equal.click();
         test1.log(Status.PASS, "Clicked on Equal Sign");
>>>>>>> 181ee96fa4b41f6dbf14cde3c514e1b63d6e771b
        String res = result.getText();
        System.out.println("\n The result is: "+res);

        //Printing results
        if (res == "8"){
        }else{
            try {
                test1.fail("miscalculation: Result should be '8', but it's actually "+res, MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());
            } catch (IOException e) {
                e.printStackTrace();
                test1.log(Status.WARNING, "Unable to load Image");
            }
        }
        test1.log(Status.INFO, "Test finished...");

    }
    @Test
    public void restTest(){
        System.out.println("Application Started...");
        // creates a toggle for the given test, adds all log events under it
        ExtentTest test2 = extent.createTest("Rest", "RestDown 2 number then validate results");
        MobileElement four = driver.findElement(By.id("com.android.calculator2:id/digit_4"));
        MobileElement rest = driver.findElement(By.id("com.android.calculator2:id/op_sub"));
        MobileElement equal = driver.findElement(By.id("com.android.calculator2:id/eq"));
        MobileElement result = driver.findElement(By.id("com.android.calculator2:id/result"));

        // log(Status, details)
        test2.log(Status.INFO, "Test started...");
        four.click();
<<<<<<< HEAD
        test2.log(Status.PASS, "Clicked on Number Four(4)");
        rest.click();
        test2.log(Status.PASS, "Clicked on Minus Sign(-)");
        four.click();
        test2.log(Status.PASS, "Clicked on Number Four(4)");
        equal.click();
        test2.log(Status.PASS, "Clicked on Equal Sign(=)");
=======
        test2.log(Status.PASS, "Clicked on Number Four");
        rest.click();
        test2.log(Status.PASS, "Clicked on Minus Sign");
        four.click();
        test2.log(Status.PASS, "Clicked on Number Four");
        equal.click();
        test2.log(Status.PASS, "Clicked on Equal Sign");
>>>>>>> 181ee96fa4b41f6dbf14cde3c514e1b63d6e771b
        String res = result.getText();
        System.out.println("\n The result is: "+res);

        //Printing results
<<<<<<< HEAD
        String expected = "0";
        if (res.equals(expected)){
            test2.log(Status.PASS, "Result is "+expected);
        }else{
=======
        if (res == "0"){
>>>>>>> 181ee96fa4b41f6dbf14cde3c514e1b63d6e771b
            try {
                test2.fail("miscalculation: Result should be '0', but it's actually "+res, MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());
            } catch (IOException e) {
                e.printStackTrace();
                test2.log(Status.WARNING, "Unable to load Image");
            }
<<<<<<< HEAD

        }

=======
        }else{
            test2.log(Status.PASS, "Result is "+res);
        }
        
>>>>>>> 181ee96fa4b41f6dbf14cde3c514e1b63d6e771b
        test2.log(Status.INFO, "Test finished...");
    }

    @AfterTest
    public void teardown(){
        System.out.println("Completed...");
    }
}
